import java.util.ArrayList;
import java.util.Scanner;

class Kermis {
    String naam;
    boolean opDeKermis;
    final static String WELKOM = "Hallo! Wat is je woonplaats?";
    ArrayList<Attractie> attracties = new ArrayList<>();
    Kassa kassa = new Kassa();
    final static BelastingInspecteur belastingInspecteur = new BelastingInspecteur("Jacques");

    public static void main(String[] args) {
        System.out.println(WELKOM);
        Scanner scanner = new Scanner(System.in);
        String woonplaats = scanner.nextLine();
        Kermis kermis = new Kermis(woonplaats);
        kermis.bezoeken();
    }

    public Kermis(String naam) {
        this.naam = naam;
        this.attracties.add(new Botsauto(1));
        this.attracties.add(new Spin(2));
        this.attracties.add(new SpiegelPaleis(3));
        this.attracties.add(new Spookhuis(4));
        this.attracties.add(new Hawaii(5));
        this.attracties.add(new LadderKlimmer(6));
    }

    void bezoeken() {
        System.out.println("Je bezoekt de kermis van " + naam + "!");
        opDeKermis = !opDeKermis;
        doenOpKermis();
    }

    private void doenOpKermis() {
        Scanner input = new Scanner(System.in);
        while (opDeKermis) {
            System.out.println("\nWelkom op het Kermisplein\n" +
                    "Kies 'i' voor informatie over de Kermis\n" +
                    "Kies 'a' om naar een Attractie te gaan\n" +
                    "Kies 'b' om je voor te doen als Belastinginspecteur en belasting te innen\n" +
                    "Kies 'h' om naar Huis te gaan en de Kermis te verlaten\n");
            String keuze = input.nextLine().trim().toLowerCase();

            if (keuze.equals("i")) {
                kassa.getInfo();
                continue;
            }
            if (keuze.equals("a")) {
                getAttractie();
                continue;
            }
            if (keuze.equals("b")) {
                attracties.forEach(belastingInspecteur::hefBelasting);
                continue;
            }
            if (keuze.equals("h")) {
                System.out.println("Namens de Kermisdirecteur : 'Bedankt en tot ziens!'");
                opDeKermis = false;
                break;
            } else {
                System.out.println("Kies een van de beschikbare opties");
            }
        }
    }

    private void getAttractie() {
        Scanner input = new Scanner(System.in);
        while (opDeKermis) {
            System.out.println("Welke attractie wil je bezoeken?");
            printAttractie();
            int keuze = input.nextInt();
            if (keuze > 0 && keuze < 7) {
                doenBijAttractie(attracties.get(keuze - 1));
                break;
            } else {
                System.out.println("Deze Attractie is tijdelijk gesloten, probeer een andere Attractie");
            }
        }
    }

    private void printAttractie() {
        attracties.forEach(attractie -> System.out.println(attractie.nummer + " : " + attractie.naam));
    }

    private void doenBijAttractie(Attractie attractie) {
        Scanner input = new Scanner(System.in);
        while (opDeKermis) {
            System.out.println("Je kunt het volgende doen bij de Attractie:\n" +
                    "Kies 'i' voor informatie over de Attractie\n" +
                    "kies 't' om een ticket te kopen voor de Attractie en te ervaren\n" +
                    "Kies 'v' om de Attractie te verlaten\n");
            if (attractie.monteurNodig) {
                System.out.println("Kies 'm' om de Attractie te laten servicen!\n");
            }
            String keuze = input.nextLine().trim().toLowerCase();
            if (keuze.equals("i")) {
                attractie.getInfo();
                continue;
            }
            if (keuze.equals("t")) {
                try {
                    attractie.draaien();
                    kassa.totaalOmzet += (attractie.omzet / attractie.verkochteKaartjes);
                    kassa.totaalTickets++;
                    continue;
                } catch (KeuringException e) {
                    System.out.println(e.getMessage());
                    attractie.monteurNodig = true;
                }
            }
            if (keuze.equals("m")) {
                if (attractie instanceof RisicoRijkeAttractie) {
                    ((RisicoRijkeAttractie) attractie).opstellingsKeuring();
                    System.out.println("De monteur is snel bij " + attractie.naam + " en voert de Keuring uit!\n");
                    attractie.monteurNodig = false;
                }
            }
            if (keuze.equals("v")) {
                System.out.println("Je gaat terug naar het Kermisplein");
                break;
            } else {
                System.out.println("Kies een van de beschikbare opties");
            }
        }
    }
}

class Kassa {
    double totaalOmzet = 0.00;
    int totaalTickets = 0;

    void getInfo() {
        Scanner input = new Scanner(System.in);
        while (true) {
            System.out.println("Kies 'k' voor het aantal kaartjes dat is verkocht op de Kermis");
            System.out.println("Kies 'o' voor omzet van de Kermis");
            System.out.println("Kies 't' om terug te gaan naar het Kermisplein");
            String keuze = input.nextLine().trim().toLowerCase();
            if (keuze.equals("k")) {
                System.out.println(totaalTickets + " ticket(s) verkocht op de Kermis\n");
                continue;
            }
            if (keuze.equals("o")) {
                System.out.println("€ " + totaalOmzet + " bedraagt de omzet van de Kermis\n");
                continue;
            }
            if (keuze.equals("t")) {
                break;
            } else {
                System.out.println("Kies een van de beschikbare opties");
            }
        }
    }
}

class Attractie {
    String naam;
    int nummer;
    double prijs;
    double omzet;
    int verkochteKaartjes;
    boolean monteurNodig;

    public Attractie(int nummer) {
        this.nummer = nummer;
    }

    void draaien() throws KeuringException {
        System.out.println("De Attractie " + naam + " draait!");
        this.omzet += this.prijs;
        verkochteKaartjes++;
    }

    void getInfo() {
        Scanner input = new Scanner(System.in);
        while (true) {
            System.out.println("Kies 'k' voor het aantal kaartjes dat is verkocht van de attractie\n" +
                    "Kies 'o' voor omzet van de attractie\n" +
                    "Kies 't' om terug te gaan naar de attractie\"");
            String keuze = input.nextLine().trim().toLowerCase();
            if (keuze.equals("k")) {
                System.out.println("Er zijn " + verkochteKaartjes + " kaartjes verkocht van de Attractie " + naam);
                continue;
            }
            if (keuze.equals("o")) {
                System.out.println("De " + naam + " heeft € " + omzet + " opgeleverd");
                continue;
            }
            if (keuze.equals("t")) {
                break;
            } else {
                System.out.println("Kies een van de beschikbare opties");
            }
        }
    }
}

class Botsauto extends Attractie {
    public Botsauto(int nummer) {
        super(nummer);
        this.naam = "Botsauto's";
        this.prijs = 2.50;
    }
}

class Spin extends RisicoRijkeAttractie implements GokAttractie {
    double kansSpelBelasting;

    public Spin(int nummer) {
        super(nummer);
        this.naam = "Spin";
        this.prijs = 2.25;
        this.risicoRijk = true;
        this.draaiLimiet = 5;
    }

    @Override
    void draaien() throws KeuringException {
        super.draaien();
        System.out.println("De Attractie " + naam + " draait!");
        this.omzet += 0.7 * this.prijs;
        this.kansSpelBelasting += 0.3 * this.prijs;
        this.verkochteKaartjes++;
    }

    @Override
    public double kansSpelBelastingBetalen() {
        double reserveren = kansSpelBelasting;
        this.kansSpelBelasting = 0.0;
        return reserveren;
    }
}

class SpiegelPaleis extends Attractie {
    public SpiegelPaleis(int nummer) {
        super(nummer);
        this.naam = "Spiegelpaleis";
        this.prijs = 2.75;
    }
}

class Spookhuis extends Attractie {
    public Spookhuis(int nummer) {
        super(nummer);
        this.naam = "Spookhuis";
        this.prijs = 3.20;
    }
}

class Hawaii extends RisicoRijkeAttractie {
    public Hawaii(int nummer) {
        super(nummer);
        this.naam = "Hawaii";
        this.prijs = 2.90;
        this.risicoRijk = true;
        this.draaiLimiet = 10;
    }
}

class LadderKlimmer extends Attractie implements GokAttractie {
    double kansSpelBelasting;

    public LadderKlimmer(int nummer) {
        super(nummer);
        this.naam = "Ladderklimmer";
        this.prijs = 5.00;
    }

    @Override
    void draaien() {
        System.out.println("De Attractie " + naam + " draait!");
        this.omzet += 0.7 * this.prijs;
        this.kansSpelBelasting += 0.3 * this.prijs;
        this.verkochteKaartjes++;
    }

    @Override
    public double kansSpelBelastingBetalen() {
        double reserveren = kansSpelBelasting;
        this.kansSpelBelasting = 0.0;
        return reserveren;
    }
}

abstract class RisicoRijkeAttractie extends Attractie {
    boolean risicoRijk;
    int draaiLimiet;
    int draaienNaKeuring;

    public RisicoRijkeAttractie(int nummer) {
        super(nummer);
    }

    @Override
    void draaien() throws KeuringException {
        if (this.draaienNaKeuring < this.draaiLimiet) {
            if (this.nummer == 2) {
                this.draaienNaKeuring++;
                return;
            }
            super.draaien();
            this.draaienNaKeuring++;
        } else {
            throw new KeuringException(this.naam);
        }
    }

    void opstellingsKeuring() {
        this.draaienNaKeuring = 0;
    }
}

class KeuringException extends Exception {
    public KeuringException(String naam) {
        super("De attractie " + naam + " moet gekeurd worden!\n");
    }
}

interface GokAttractie {
    double kansSpelBelastingBetalen();
}

class BelastingInspecteur {
    String naam;
    double geindeBelasting;

    public BelastingInspecteur(String naam) {
        this.naam = naam;
    }

    void hefBelasting(Attractie attractie) {
        if (attractie instanceof GokAttractie) {
            GokAttractie gokAttractie = (GokAttractie) attractie;
            System.out.println("Belastinginspecteur " + naam + " int de belasting bij " + attractie.naam);
            double belasting = gokAttractie.kansSpelBelastingBetalen();
            this.geindeBelasting += belasting;
            System.out.println("€ " + belasting + " is er geind bij " + attractie.naam + "\n" +
                    "€ " + geindeBelasting + " is er in totaal geind\n");
        }
    }
}
