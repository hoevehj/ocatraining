import java.util.*;

class YahtzeeSpel {
    final static List<Integer> geblokkeerd = new ArrayList<>(List.of(1, 1, 1, 1, 1));
    final List<Dobbelsteen> dobbelstenen = new ArrayList<>();
    final List<Integer> blokkeren = new ArrayList<>(List.of(0, 0, 0, 0, 0));
    final List<Speler> spelers = new ArrayList<>();
    int[] laatsteWorp = new int[5];

    public YahtzeeSpel() {
        for (int i = 0; i < 5; i++) {
            dobbelstenen.add(new Dobbelsteen());
        }
    }

    public static void main(String[] args) {
        new YahtzeeSpel().spelen();
    }

    public void spelen() {
        Scanner scanner = new Scanner(System.in);
        System.out.println(Bericht.INTRO);
        System.out.println(Bericht.SPELERS);
        int aantalSpelers = scanner.nextInt();
        for (int i = 0; i < aantalSpelers; i++) {
            int a = i + 1;
            Scanner r = new Scanner(System.in);
            System.out.println("Speler" + a);
            System.out.println(Bericht.WELKOM);
            String spelerNaam = r.nextLine();
            spelers.add(new Speler(spelerNaam));
            System.out.println("Hallo " + spelers.get(i).naam + "\n");
        }
        for (Speler speler : spelers) {
            Scanner r = new Scanner(System.in);
            System.out.println("Speler " + speler.naam + " is aan de beurt!\n");
            for (; true; ) {
                System.out.println(Bericht.KEUZE);
                String keuze = r.nextLine();
                if (keuze.equals("q")) {
                    break;
                }
                if (keuze.equals("")) {
                    Worp worp = new Worp();
                    laatsteWorp = worp.getWorp(dobbelstenen, blokkeren, laatsteWorp);
                    speler.geschiedenis.add(worp);
                    System.out.println("WORP" + speler.geschiedenis.size());
                    System.out.println(worp.getUitslag());
                    reset(blokkeren);
                    vasthouden();
                    if (blokkeren.equals(geblokkeerd)) {
                        System.out.println(Bericht.ALLESVASTHOUDEN);
                        final boolean yahtzee = Arrays.stream(laatsteWorp).distinct().count() == 1;
                        if (yahtzee) {
                            System.out.println(Bericht.YAHTZEE + laatsteWorp[0]);
                        } else {
                            System.out.println(Bericht.GEENYAHTZEE);
                            System.out.println(worp.getUitslag());
                        }
                        break;
                    }
                } else {
                    System.out.println(Bericht.ONGELDIGEKEUS);
                }
            }
            System.out.println(Bericht.TOTZIENS + speler.naam + " na " + speler.geschiedenis.size() + " worpen!\n");
            reset(blokkeren);
        }
        for (Speler speler : spelers) {
            System.out.println(speler.naam + " heeft " + speler.geschiedenis.size() + " maal geworpen en de laatste worp was \n" + speler.geschiedenis.get(speler.geschiedenis.size() - 1).getUitslag());
        }
        System.out.println(Bericht.EINDE);
    }

    private void vasthouden() {
        Scanner scanner = new Scanner(System.in);
        System.out.println(Bericht.VASTHOUDEN);
        String input = scanner.nextLine();
        if (input.equals("0")) {
            System.out.println(Bericht.NIETSVASTHOUDEN);
        } else {
            for (int i = 0; i < input.length(); i++) {
                char str = input.charAt(i);
                int value = Integer.parseInt(String.valueOf(str));
                blokkeren.set(value - 1, 1);
            }
        }
    }

    private void reset(List<Integer> blokkeren) {
        for (Integer integer : blokkeren) {
            blokkeren.set(blokkeren.indexOf(integer), 0);
        }
    }
}

class Dobbelsteen {
    public int werpen() {
        return new Random().nextInt(6) + 1;
    }
}

class Worp {
    int[] geworpen = new int[5];

    public String getUitslag() {
        StringBuilder uitslag = new StringBuilder();
        for (int value : geworpen) {
            uitslag.append(value).append(" ");
        }
        return "1 2 3 4 5\n" + uitslag + "\n";
    }

    public int[] getWorp(List<Dobbelsteen> dobbelstenen, List<Integer> blokkeren, int[] laatsteWorp) {
        for (Dobbelsteen dobbelsteen : dobbelstenen) {
            if (blokkeren.get(dobbelstenen.indexOf(dobbelsteen)) == 1) {
                geworpen[dobbelstenen.indexOf(dobbelsteen)] = laatsteWorp[dobbelstenen.indexOf(dobbelsteen)];
            } else {
                geworpen[dobbelstenen.indexOf(dobbelsteen)] = dobbelsteen.werpen();
            }
        }
        return geworpen;
    }
}

class Speler {
    final String naam;
    ArrayList<Worp> geschiedenis = new ArrayList<>();

    public Speler(String naam) {
        this.naam = naam;
    }
}

class Bericht {
    final static String INTRO = "\nYAHTZEE....It makes you THINK while having FUN\n";
    final static String WELKOM = "Welkom bij Yahtzee!\nWat is uw naam?";
    final static String SPELERS = "Hoeveel spelers gaan proberen om Yahtzee te gooien?";
    final static String KEUZE = "<Enter> = gooi dobbelstenen, 'Q' om spel te stoppen";
    final static String ONGELDIGEKEUS = "Dit is niet een geldige keus, probeer het nog eens";
    final static String VASTHOUDEN = "Welke posities wilt u vasthouden? 0 voor geen anders bv 124";
    final static String NIETSVASTHOUDEN = "Je gaat alle dobbelstenen opnieuw gooien!";
    final static String ALLESVASTHOUDEN = "Alle posities zijn vastgehouden! Geen worp meer dus!";
    final static String YAHTZEE = "YAHTZEE! en dat met 5x een ";
    final static String GEENYAHTZEE = "Geen Yahtzee, eindresultaat is de worp";
    final static String TOTZIENS = "Bedankt voor het spelen van Yahtzee ";
    final static String EINDE = "Einde potje Yahtzee......";
}
