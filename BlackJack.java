import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class BlackJack {
    public static void main(String[] args) {
        Spel spel = new Spel();
        spel.spelen();
    }
}

class Spel {
    double spelerGeld = 100.00;
    double spelerInzet;
    Stapel spelerStapel = new Stapel();
    Stapel dealerStapel = new Stapel();
    Stapel deelStapel = new Stapel();
    boolean eindeRonde = false;
    boolean eindeSpel = false;

    public void spelen() {
        System.out.println("Welkom bij BlackJack");
        deelStapel.maakSpeelStapelEnSchud();

        // Spel begint en loopt zolang speler geld heeft om in te zetten en het spel niet beeindigd wordt door speler
        while (spelerGeld > 0 && !eindeSpel) {
            System.out.println("Nieuwe ronde, nieuwe kansen!\n");
            spelerInzet = inzetten();
            startKaarten();
            // Speler speelt vanaf hier
            while (true) {
                eindeRonde = false;
                System.out.println("Jouw kaarten:");
                System.out.print(spelerStapel.toString());
                System.out.println("Je kaarten hebben een waarde van: " + spelerStapel.kaartWaarde() + "\n");
                if (spelerStapel.kaartWaarde() == 21) {
                    System.out.println("Je hebt BLACKJACK! 21!\n");
                }
                System.out.println("Dealer kaarten:" + dealerStapel.neemKaart(0).toString() + " en [niet zichtbaar]\n");

                Scanner kiezen = new Scanner(System.in);
                System.out.println("Kies een van de volgende opties: (k)kaart (p)pas (s)stoppen");
                String keuze = kiezen.next();

                if (keuze.equals("k")) {
                    spelerStapel.geefKaart(deelStapel);
                    System.out.println("Je krijgt een: " + spelerStapel.neemKaart(spelerStapel.stapelGrootte() - 1));
                    if (spelerStapel.kaartWaarde() > 21) {
                        System.out.println("Jouw kaarten:");
                        System.out.print(spelerStapel.toString());
                        System.out.println("Helaas! over 21 heen, de kaarten hebben een waarde van: " + spelerStapel.kaartWaarde() + "\n");
                        spelerGeld -= spelerInzet;
                        if (spelerGeld == 0) {
                            System.out.println("Bankroet!!! Je saldo is namelijk: " + spelerGeld + "\n");
                        }
                        eindeRonde = true;
                        break;
                    }
                    continue;
                }
                if (keuze.equals("p")) {
                    System.out.println("Je past met een kaartwaarde van: " + spelerStapel.kaartWaarde() + "\n");
                    break;
                }
                if (keuze.equals("s")) {
                    eindeRonde = true;
                    eindeSpel = true;
                    break;
                } else {
                    System.out.println("Geen geldige keus gemaakt\n");
                }
            }
            // Dealer speelt vanaf hier
            while (!eindeRonde) {
                System.out.println("Dealer Kaarten: \n" + dealerStapel.toString());
                if ((dealerStapel.kaartWaarde() > spelerStapel.kaartWaarde()) && !eindeRonde) {
                    System.out.println("Dealer kaarten hebben een waarde van: " + dealerStapel.kaartWaarde());
                    System.out.println("Dealer wint!");
                    spelerGeld -= spelerInzet;
                    eindeRonde = true;
                }
                while ((dealerStapel.kaartWaarde() < 17) && !eindeRonde) {
                    dealerStapel.geefKaart(deelStapel);
                    System.out.println("Dealer krijgt een: " + dealerStapel.neemKaart(dealerStapel.stapelGrootte() - 1).toString() + "\n");
                    System.out.println("Dealer Kaarten: \n" + dealerStapel.toString());
                    System.out.println("Dealer kaarten hebben een waarde van: " + dealerStapel.kaartWaarde() + "\n");

                }
                if ((dealerStapel.kaartWaarde() > 21) && !eindeRonde) {
                    System.out.println("Jij hebt: " + spelerStapel.kaartWaarde() + " en Dealer heeft: " + dealerStapel.kaartWaarde());
                    System.out.println("Dealer heeft teveel! Jij wint.");
                    spelerGeld += spelerInzet;
                    eindeRonde = true;
                }
                if ((spelerStapel.kaartWaarde() == dealerStapel.kaartWaarde()) && !eindeRonde) {
                    System.out.println("Jij hebt: " + spelerStapel.kaartWaarde() + " en Dealer heeft: " + dealerStapel.kaartWaarde());
                    System.out.println("Gelijke stand!");
                    eindeRonde = true;
                }
                if ((spelerStapel.kaartWaarde() > dealerStapel.kaartWaarde()) && !eindeRonde) {
                    System.out.println("Jij hebt: " + spelerStapel.kaartWaarde() + " en Dealer heeft: " + dealerStapel.kaartWaarde());
                    System.out.println("Je wint!");
                    spelerGeld += spelerInzet;
                    eindeRonde = true;
                } else if (!eindeRonde) {
                    System.out.println("Jij hebt: " + spelerStapel.kaartWaarde() + " en Dealer heeft: " + dealerStapel.kaartWaarde());
                    System.out.println("Je verliest!");
                    spelerGeld -= spelerInzet;
                    eindeRonde = true;
                }
                System.out.println("Einde van deze ronde");
                System.out.println("Je hebt nu: € " + spelerGeld);
                if (spelerGeld == 0) {
                    System.out.println("Bankroet!!! Je saldo is namelijk: " + spelerGeld + "\n");
                }
            }
            // Bij ronde einde krijgt speler keuze om nog een ronde te spelen of spel te beeindigen wanneer hij nog saldo heeft
            while (spelerGeld > 0 && !eindeSpel) {
                Scanner invoer = new Scanner(System.in);
                System.out.println("Kies (n)nogmaals of (e)einde");
                String keuze = invoer.nextLine().trim();
                if (keuze.equals("n")) {
                    spelerStapel.resetStapel(deelStapel);
                    dealerStapel.resetStapel(deelStapel);
                    eindeRonde = true;
                    break;
                }
                if (keuze.equals("e")) {
                    eindeRonde = true;
                    eindeSpel = true;
                    break;
                } else {
                    System.out.println("Geen geldige keus gemaakt\n");
                }
            }
        }
        System.out.println("Einde spel!");
    }

    public double inzetten() {
        Scanner input = new Scanner(System.in);
        while (true) {
            System.out.println("Hoeveel van de € " + spelerGeld + " wil je inzetten?");
            double inleg = input.nextDouble();
            if (inleg > spelerGeld) {
                System.out.println("Je zet meer in dan je nog hebt. Kies een lager bedrag");
                continue;
            }
            return inleg;
        }
    }

    public void startKaarten() {
        spelerStapel.geefKaart(deelStapel);
        spelerStapel.geefKaart(deelStapel);
        dealerStapel.geefKaart(deelStapel);
        dealerStapel.geefKaart(deelStapel);
    }
}

class Stapel {
    private ArrayList<Kaart> kaarten;

    public Stapel() {
        this.kaarten = new ArrayList<>();
    }

    public void maakSpeelStapelEnSchud() {
        for (Soort kaartSoort : Soort.values()) {
            for (Waarde kaartWaarde : Waarde.values()) {
                this.kaarten.add(new Kaart(kaartSoort, kaartWaarde));
            }
        }
        schudden();
    }

    private void schudden() {
        ArrayList<Kaart> tijdelijkeStapel = new ArrayList<>();
        Random random = new Random();
        int kaartIndex;
        int stapelGrootte = this.kaarten.size();
        for (int i = 0; i < stapelGrootte; i++) {
            kaartIndex = random.nextInt((this.kaarten.size() - 1) + 1);
            tijdelijkeStapel.add(this.kaarten.get(kaartIndex));
            this.kaarten.remove(kaartIndex);
        }
        this.kaarten = tijdelijkeStapel;
    }

    public String toString() {
        StringBuilder kaartWaarden = new StringBuilder();
        for (Kaart kaart : this.kaarten) {
            kaartWaarden.append(kaart.toString()).append("\n");
        }
        return kaartWaarden.toString();
    }

    public void verwijderKaart(int i) {
        this.kaarten.remove(i);
    }

    public Kaart neemKaart(int i) {
        return this.kaarten.get(i);
    }

    public void toevoegenKaart(Kaart kaart) {
        this.kaarten.add(kaart);
    }

    public void geefKaart(Stapel stapel) {
        this.kaarten.add(stapel.neemKaart(0));
        stapel.verwijderKaart(0);
    }

    public int stapelGrootte() {
        return this.kaarten.size();
    }

    public void resetStapel(Stapel stapel) {
        int stapelGrootte = this.kaarten.size();

        for (int i = 0; i < stapelGrootte; i++) {
            stapel.toevoegenKaart(this.neemKaart(i));
        }

        for (int i = 0; i < stapelGrootte; i++) {
            this.verwijderKaart(0);
        }
    }

    public int kaartWaarde() {
        int totaleWaarde = 0;
        int aantalAzen = 0;

        for (Kaart kaart : this.kaarten) {
            switch (kaart.geefWaarde()) {
                case TWEE:
                    totaleWaarde += 2;
                    break;
                case DRIE:
                    totaleWaarde += 3;
                    break;
                case VIER:
                    totaleWaarde += 4;
                    break;
                case VIJF:
                    totaleWaarde += 5;
                    break;
                case ZES:
                    totaleWaarde += 6;
                    break;
                case ZEVEN:
                    totaleWaarde += 7;
                    break;
                case ACHT:
                    totaleWaarde += 8;
                    break;
                case NEGEN:
                    totaleWaarde += 9;
                    break;
                case TIEN:
                case BOER:
                case HEER:
                case VROUW:
                    totaleWaarde += 10;
                    break;
                case AAS:
                    aantalAzen += 1;
                    break;
            }
        }

        for (int i = 0; i < aantalAzen; i++) {
            if (totaleWaarde > 10) {
                totaleWaarde += 1;
            } else {
                totaleWaarde += 11;
            }
        }
        return totaleWaarde;
    }
}

class Kaart {
    private final Soort soort;
    private final Waarde waarde;

    public Kaart(Soort soort, Waarde waarde) {
        this.soort = soort;
        this.waarde = waarde;
    }

    public String toString() {
        String symbool = "";
        switch (this.geefSoort()) {
            case SCHOPPEN:
                symbool = "\u2660";
                break;
            case KLAVER:
                symbool = "\u2663";
                break;
            case HARTEN:
                symbool = "\u2665";
                break;
            case RUITEN:
                symbool = "\u2666";
                break;
        }
        return symbool + "-" + this.waarde.toString();
    }

    public Soort geefSoort() {
        return this.soort;
    }

    public Waarde geefWaarde() {
        return this.waarde;
    }
}

enum Soort {
    HARTEN, SCHOPPEN, KLAVER, RUITEN
}

enum Waarde {
    TWEE, DRIE, VIER, VIJF, ZES, ZEVEN, ACHT, NEGEN, TIEN, BOER, HEER, VROUW, AAS
}
